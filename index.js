const express = require('express');
const uuid = require('uuid');

const app = express();

app.use(express.json());

const baseMusicas = [];

function cadastrarMusica(nome) {
  const musicaExistente = baseMusicas.find((musica) => musica.nome === nome);
  if (musicaExistente) {
    return "Música já cadastrada!";
  }

  const musicaNova = {
    id: uuid.v4(),
    nome: nome,
    avaliacao: 0
  };
  baseMusicas.push(musicaNova);
  return musicaNova;
}

function listarMusicas() {
    const musicasOrdenadas = baseMusicas.slice().sort((a, b) => {
      if (a.avaliacao !== b.avaliacao) {
        return b.avaliacao - a.avaliacao; // Ordenar por avaliação decrescente
      }
      return a.nome.localeCompare(b.nome); // Ordenar por nome em ordem alfabética
    });
  
    return musicasOrdenadas.map((musica) => ({
      nome: musica.nome,
      avaliacao: musica.avaliacao
    }));
  }
  
function avaliarMusica(nome, avaliacao) {
  avaliacao = Number(avaliacao);
  if (avaliacao <= 5 && avaliacao >= 1) {
    const musica = baseMusicas.find((musica) => musica.nome === nome);
    if (musica) {
      musica.avaliacao = avaliacao;
      return musica;
    }
    return "Música não existe!";
  } else {
    return "A avaliação deve ser de 1 a 5";
  }
}

app.post('/cadastrar', (req, res) => {
  const musicaCadastrada = cadastrarMusica(req.body.nome);
  res.status(200).send(musicaCadastrada);
});

app.get('/listar', (req, res) => {
  const musicas = listarMusicas();
  res.status(200).send(musicas);
});

app.post('/avaliar', (req, res) => {
  const musicaAvaliada = avaliarMusica(req.body.nome, req.body.avaliacao);
  res.status(200).send(musicaAvaliada);
});

app.get('/teste', (req, res) => {
  res.status(200).send("Funcionando!");
});

app.listen(4200, () => {
  console.log("Avaliação de Músicas. Porta 4200");
});